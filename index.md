---
marp: true

theme: default
header: ''
footer: ''
backgroundImage: url('./styles/background.png')
title: Vorlesung Verbundwerkstoffe
author: Christian Willberg
---



<script type="module">
  import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
  mermaid.initialize({ startOnLoad: true });
</script>

<style>
.container{
  display: flex;
  }
.col{
  flex: 1;
  }
</style>

<style scoped>
.column-container {
    display: flex;
    flex-direction: row;
}

.column {
    flex: 1;
    padding: 0 20px; /* Platzierung der Spalten */
}

.centered-image {
    display: block;
    margin: 0 auto;
}
</style>

<style>
footer {
    font-size: 14px; /* Ändere die Schriftgröße des Footers */
    color: #888; /* Ändere die Farbe des Footers */
    text-align: right; /* Ändere die Ausrichtung des Footers */
}
</style>


## Vorlesung Werkstofftechnik
Prof. Dr.-Ing.  Christian Willberg<a href="https://orcid.org/0000-0003-2433-9183"><img src="styles/ORCIDiD_iconvector.png" alt="ORCID Symbol" style="height:15px;width:auto;vertical-align: top;background-color:transparent;"></a>
Hochschule Magdeburg-Stendal

![bg right](./Figures/kohlefaserbeispiel.png)

Kontakt: christian.willberg@h2.de

<div style="position: absolute; bottom: 10px; left: 520px; color: blue; font-size: 20px;"> 
    <a href="https://leichtbau.dlr.de" style="color: blue;">Bildreferenz</a>
</div>


---

<!--paginate: true-->

![bg  98% fit](./Figures/kohlefaserbeispiel.png)

![bg  70% fit](./styles/qr-code.png)



---

![bg fit](./Figures/verbundmaterial.png)



---

![](./Figures/FaserverbundBeispiele.png)

---

## Anwendungen



| Gebiet  | Motivation | 
|---|---|
| Luft- und Raumfahrt  | Festigkeits/Steifigkeits – Gewichtsverhältnis, Schadens- und  Korrosionsstoleranz, Wartbarkeit / Reparierbarkeit | 
| Schifffahrt  | Salzwasser; Schlagschäden, Lange Lebendauer | 
| Bauwesen  | Widerstand gegen Umwelteinflüsse und Korrosion; Langlebigkeit | 
|Landtransportsysteme | Kosten, Einfache Fertigbarkeit | 

---
## Vorteile & Nachteile
| Vorteile  | Nachteile | 
|---|---|
| Spezifische Materialeigenschaften | Variantenreichtum|
| Weitgehend elastisches Verhalten|Geringe Zugfestigkeit senkrecht zur Faser |
| Hohe Festigkeiten und Steifigkeiten in Faserrichtung | Schwierige Prüfung |
| „maßgeschneidertes“ Material |  Recyclebarkeit | 
| Alterungs- und Korrosionsbeständigkeit | Spezifische Materialeigenschaften und Verarbeitungsverfahren |
|  | Kein plastisches Verhalten |
---

## Komponenten von Faserkunstoffverbunde 
**Matrix – bindende Komponente**
- Formgebend
- Schutz und Stabilisierung der Fasern
- Spannungen an die Fasern übertragen

**Faser – verstärkende Komponente**
- Lasttragend da hohe Steifigkeit und/oder - Festigkeit bei geringem Gewicht
- Begrenzte thermische Dehnung

**Weitere Bestandteile (optional)**
- Faserbeschichtungen
- Füllstoffe
- Beimischung anderer Fasertypen

---

## Ausgangswerkstoffe - Faser

- Kurzfasern
    - Wirre Anordnung (geringe Anistropie)
    - Oft recyclete Fasern
- Langfasern
    - Mehrere Millimeter Länge
    - Geringere Anforderung bei Verarbeitung und Lagerung
- Endlosfasern
    - Hohe Festigkeiten und Steifigkeiten
    - Höhere Anforderung bei Verarbeitung und Lagerung
---

- Naturfasern: Haare, Wolle, Seide, Baumwolle, Flachs, Sisal, Hanf, Jute, Ramie, Bananenfasern ...
- Organische Fasern: Polyethylen (PE), Polypropylen (PP), Polyamid (PA), Polyester (PES), Polyacrylnitril (PAN), Aramid, Kohlenstoff ...
- Anorganische Fasern: Glas, Basalt, Quarz, SiC, Al2O3, Bor, ...
Metallfasern aus: Stahl, Aluminium, Kupfer, Nickel, Beryllium, Wolfram ... 

![](Figures/faser_zoom.png)

---

## Koordinaten
![](./Figures/koordinaten.png)

---

## Eigenschaften

| Material | $E_{f,11}\,[N/mm^2]$ | $E_{m}\,[N/mm^2]$ | $E_{11}\,[N/mm^2]$ |
|---|---|---|---|
E-Glasfaser | 73000 | 3400 | 45160  | 
HT-C-Faser | 230000 | 3400 | 139960 |
HM-C-Faser | 392000 | 3400 | 236560 |
Aramamid   | 125000 |3400 | 76360 |
Stahl 25CrMo4 | | | 206000|
Aluminium-Legierung AlCuMg2 | | | 72400|
Titan-Legierung | | | 108000|

---

**Faservolumenanteil**
$\rho = \phi\rho_f+(1-\phi)\rho_m$
- Prozessabhängig

![](./Figures/quadratisch.svg)

![](./Figures/hexagonal.svg)

![bg right 80% ](./Figures/faser_mikrostruktur.png)

---

$A_{Quadrat} = (2R)^2 = 4\cdot R^2$
$A_{Kreis} = \pi R^2$

$\rho_{f,max}=\frac{A_{Kreise}}{A_{Quadrat}}=\frac{\pi}{4}$

quadratische Packung
$\phi_{max}=\frac{\pi}{4}\approx 0.79$

hexagonale Packung
$\phi_{max}=\frac{\pi}{\sqrt{12}}\approx 0.91$

---

## Glasfasern

**Vorteile**
- hohe Längs-Zug- sowie die hohe Längs-Druckfestigkeit
- Eine hohe Bruchdehnung
- aufgrund der niedrigen Fasersteifigkeit gute Drapierbarkeit, auch um enge Radien
- die vollkommene Unbrennbarkeit
- die sehr geringe Feuchtigkeitsaufnahme
- die gute chemische und mikrobiologische Widerstandsfähigkeit
- geringe Kosten
---

**Nachteile**
- der für viele Strukturbauteile zu niedrige Elastizitätsmodul der Glasfaser
- Glasfasern sind unverrottbar (Vor- und Nachteil)

---

## Kohlefasern
**Vorteil**
- C-Fasern sind sehr leicht, ihre Dichte ($𝜌_𝑓 ≈ 1.8 g/cm^33$) liegt deutlich unter derjenigen von Glasfasern ($𝜌_𝑓 ≈ 2.54 g/cm^3$). 
- extrem hohe Festigkeiten und sehr hohe Elastizitätsmoduln
- beide mechanischen Größen sind zudem in weiten Bereichen bei der Herstellung der Fasern einstellbar
- Exzellente Ermüdungsfestigkeit

---

**Nachteile**
- Geringere Druckfestigkeit in Faserrichtung
- Schlechtere Drapierbarkeit
- Kosten
- Elastizitätsmoduln in Faserlängs- und Querrichtung unterscheiden sich um eine Größenordnung (Vor- und Nachteil)

---

## Fertigung
- Wahl des Verfahrens hängt  ab von
- Der konkreten Anwendung
- Einsatzbedingungen
- Grundmaterial (Faser, Matrix)
- Stückzahl
- Kosten
- Betriebsicherheit
- …

---
## Formwerkzeug / Faserablage

![](./Figures/Formwerkzeug.png)

---

## Halbzeuge

![](./Figures/Gewebe.png)

---


![](./Figures/sandwich.png)

---

## Faserablage

![](./Figures/faserablage.png)

---

## Manuelle Fertigung

![](./Figures/handlaminieren.png)

---

## Infusionsverfahren & RTM (Resin Transfer Moulding) Verfahren

![](./Figures/RTM.png)

---


![](./Figures/Autoklav.png)

---

## Wickeln


![](./Figures/wickeln.png)

---

## Automatisierte Faserablageverfahren
![](./Figures/AFP.png)


---

## Schäden in Faserverbunden

Es können 52 Fehlertypen kategorisiert werden 
- Einteilung nach Auftreten im Lebenszyklus
    - Materialprozess – Fehler die bei der Bereitstellung der Teilbestandteile auftreten
    - Komponentenfertigung – Fehler während der Kompositfertigung, welche während der Ablage, Aushärtung, Bearbeitung oder der Assemblierung auftreten
    - In-service Nutzung – im Betrieb auftretende Schäden
- Einteilung nach Größe
    - Mikroskopisch 
    - Makroskopisch

---

![](./Figures/delamination.png)

---

![](./Figures/poren.png)

---

![](./Figures/Bohrungen.png)

---
## Prüfung

![](./Figures/Prüfung.png)


---

## Lagenaufbau

- Unidirektional (UD) $[0°]_i$; $[0°]_{10}$;

- quasi isotrop $[0°\,+45°\,-45°\,90°]_s$

- Biaxiales Gelege $[+60°\,-60°]$, $[+45°\,-45°]_s$

- Triaxiales Gelege $[+60°\,-60°\,0°]$

- Torsion  $[+45°\,-45°]$

- beliebig  $[0°\,10°\,45°\,90°\,0°]_{22}$

---

## Klassische Laminattheorie

- Annahmen:
    - linear elastische Einzellagen
    - das Laminat ist dünn
    - $\sigma_z=\sigma_{xz}=\tau_{yz}=0$ 
[Guter Überblick](https://www.maschinenbau.tu-darmstadt.de/media/lsm/buchveroeffentlichungen/Strukturmechanik_ebener_Laminate_LESEPROBE.pdf)
---

## Modell

$$\left[\begin{matrix}\mathbf{F}\\
\mathbf{M}\end{matrix}\right]=\left[\begin{matrix}
    \mathbf{A} & \mathbf{B} \\
    \mathbf{B} & \mathbf{D}
\end{matrix}\right]\left[\begin{matrix}\boldsymbol{\varepsilon}\\
\boldsymbol{\kappa}\end{matrix}\right]$$

---
## Scheibensteifigkeit
$A_{ij}=\sum_{k=1}^N=\hat{Q}_{ij,k}t_k$

![](https://upload.wikimedia.org/wikipedia/de/8/82/Dehnung-Schiebungs-Kopplung.png)

isotropes Material $Q_{11}=\frac{E}{1-\nu^2}$


---

## Plattensteifigkeit
$D_{ij}=\sum_{k=1}^N=\hat{Q}_{ij,k}t_k\left(\frac{t^2_k}{12}+\left(z_k - \frac{t_k}{2} \right)^2 \right)$


![](https://upload.wikimedia.org/wikipedia/de/b/bc/Biege-Drill-Kopplung.png)

---

## Koppelsteifigkeit
$B_{ij}=-\sum_{k=1}^N=\hat{Q}_{ij,k}t_k\left(z_k - \frac{t_k}{2} \right)$
![](https://upload.wikimedia.org/wikipedia/commons/b/bb/Dehnung-Kr%C3%BCmmungs-Kopplung.png)

---

## Homogenisierungen
**Ingenieurskonstanten**
$E_x=\frac{1}{A_{11}t_{ges}}$
$E_y=\frac{1}{A_{22}t_{ges}}$
$G_{xy}=\frac{1}{A_{66}t_{ges}}$
$\nu_{yx}=\frac{A_{12}}{A_{11}}\neq\nu_{xy}=\frac{A_{12}}{A_{22}}$

--- 
# Übungen

- Mischgelege 
    - Glasfaser 3 Lagen; Kohlefaser 5 Lagen je 0°
    - Moduli bestimmen
- Lagenaufbau Kohlefaser; 
    - $t = 0.2 mm$ und $t=0.125mm$
    - [0 90 90 0 0 90 90 0]
---


# Auslegung von Faserverbunden

- Zwischenfaserbruch
- Matrixbruch
- Faserbruch
- Stabilität
- Schlagfestigkeit
- Ermüdung (tritt selten auf, oder wird vernachlässigt)

---

- Maximalspannungskriterium
- Maximaldehnungskriterium
- Versagenskriterium nach Puck, Tsai-Wu, Hashin, u.a.

- First ply vs. Last ply failure

---

## Auslegung
$$\left[\begin{matrix}
    \mathbf{A} & \mathbf{B} \\
    \mathbf{B} & \mathbf{D}
\end{matrix}\right]^{-1}\left[\begin{matrix}\mathbf{F}\\
\mathbf{M}\end{matrix}\right]=\left[\begin{matrix}\boldsymbol{\varepsilon}\\
\boldsymbol{\kappa}\end{matrix}\right]$$

$$\boldsymbol{\sigma}_k = \mathbf{Q}_k\boldsymbol{\varepsilon}$$


- Festigkeit der Einzellagen

---


## Beipiele

**Kragbalken**
- Steifigkeitsgerechte Auslegung
- Festigkeitsauslegung
    - für verschiedene Profile
    - für Aluminium, Titan, Stahl
    - für Beispiellaminate

https://tu-dresden.de/ing/maschinenwesen/ilr/lft/elamx2/elamx

---

## Vorgehen


- Profil (Kasten, Vollprofil)
    - Flächenträgheitsmoment + Satz von Steiner
- Biegelinie
    - max $w$ und $EI$ bestimmen
- maximale Biegespannung Kragbalken
    - gegen Festigkeit prüfen
    - mit ElamX berechnen

---

# Leichtbauexkurs
- Leichtbau nutzt die geometrischen Steifigkeiten
- Stringer, Sandwich
- Stabtragwerke
![](https://upload.wikimedia.org/wikipedia/commons/4/45/D-Box_Mini.jpg)