## Vorlesung für Werkstofftechnik


* [Vorlesungsskript](https://verbundwerksstoffe-lectures1000308-material-scie-758aa074e5a4f3.gitlab.io/)

## Wie zu zitieren
Über ein bib

 @techreport{WillbergC_material_science_05,
    title       = "{Vorlesung Werkstofftechnik}",
    author      = "Willberg, Christian",
    institution = "Hochschule Magedeburg-Stendal",
    address     = "Magdeburg, Germany",
    number      = "6",
    year        = 2024,
  }